package ru.rosnou.bank.client;


public class JurePerson extends Client {
    final static double TAKE_COMMISSION = 0.01;
    final static double ADD_COMMISSION = 0;

    JurePerson( long money ){
       super(money);
    }

    @Override
    protected double getAddingCommision(long money) {
        return ADD_COMMISSION;
    }

    @Override
    protected double getTakingCommision(long money) {
        return TAKE_COMMISSION;
    }

}
