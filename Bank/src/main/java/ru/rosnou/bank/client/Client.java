package ru.rosnou.bank.client;

public abstract class Client {
    protected long moneyCount;

    public Client(long moneyCount) {
        this.moneyCount = moneyCount;
    }

    protected long getMoneyCount() {
        return moneyCount;
    }

    public void addMoney(long money) {
        moneyCount += money * (1 - getAddingCommision(money));
    }

    public void takeMoney(long money) {
        moneyCount -= money * (1 + getTakingCommision(money));
    }

    abstract protected double getAddingCommision(long money);

    abstract protected double getTakingCommision(long money);


}
