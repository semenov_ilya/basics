package ru.rosnou.bank.client;


public class PhysPerson extends Client {
    final static double TAKE_COMMISSION = 0;
    final static double ADD_COMMISSION = 0;

    public PhysPerson(long moneyCount) {
        super(moneyCount);
    }


    @Override
    protected double getAddingCommision(long money) {
        return ADD_COMMISSION;
    }

    @Override
    protected double getTakingCommision(long money) {
        return TAKE_COMMISSION;
    }

}

