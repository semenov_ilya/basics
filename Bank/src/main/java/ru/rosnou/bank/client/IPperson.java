package ru.rosnou.bank.client;


public class IPperson extends Client {
    final static int CRITICAL_MONEY = 100000;
    final static double ADD_LEAST_COMMISSION = 0.005;
    final static double ADD_GREATEST_COMMISSION = 0.01;
    final static double TAKE_COMMISSION = 0.01;

    public IPperson(long moneyCount) {
        super(moneyCount);
    }


    @Override
    protected double getAddingCommision(long money) {
        double comission = 0;
        if (money > 0) {
            comission = (money < CRITICAL_MONEY ? ADD_LEAST_COMMISSION : ADD_GREATEST_COMMISSION);
        }
        return comission;
    }

    @Override
    protected double getTakingCommision(long money) {
        return (money > 0 ? TAKE_COMMISSION : null);
    }
}
