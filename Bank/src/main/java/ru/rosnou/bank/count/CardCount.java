package ru.rosnou.bank.count;


public class CardCount extends BankCount {
final private double WITHDRAWAL_COMMISSION = 0.01;
final private double DEPOSIT_COMMISION = 0;

    public CardCount(long money) {
        moneyCount = money;
    }

    @Override
    protected double getDepositComission() {
        return DEPOSIT_COMMISION;
    }

    @Override
    protected double getWithdrawalComission() {
        return WITHDRAWAL_COMMISSION;
    }
}
