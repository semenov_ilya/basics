package ru.rosnou.bank.count;

public abstract class BankCount {
    protected static long moneyCount;

    protected abstract double getDepositComission();
    protected abstract double getWithdrawalComission();

    protected long getBalance() {
        return moneyCount;
    }

    protected void addMoney(long money) {
        moneyCount += money * (1 - getDepositComission());
    }

    protected void takeMoney(long money) {
        moneyCount -= money * (1 + getWithdrawalComission());
    }
}


