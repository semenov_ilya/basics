package ru.rosnou.bank.count;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DebetCount extends BankCount {
    private Date startDate;

    final private double WITHDRAWAL_COMMISSION = 0.01;
    final private double DEPOSIT_COMMISION = 0;

    public DebetCount(long money) {
        moneyCount = money;
        startDate = new Date();
    }

    @Override
    protected double getDepositComission() {
        return DEPOSIT_COMMISION;
    }

    @Override
    protected double getWithdrawalComission() {
        return WITHDRAWAL_COMMISSION;
    }

    @Override
    public void addMoney(long money) {
        super.addMoney(money);
        startDate = new Date();
    }

    @Override
    public void takeMoney(long money) {
        if (money > 0) {
            Date endDate = new Date();
            if (!afterMonth(startDate, endDate)) {
                System.out.println("You should wait at least month after last adding.");
            } else {
                super.takeMoney(money);
                startDate = endDate;
            }
        }
    }

    private boolean afterMonth(Date start, Date end) {
        long dur = end.getTime() - start.getTime();
        int deltaTime = (int) TimeUnit.MILLISECONDS.toDays(dur);
        return deltaTime > 30;
    }
}


